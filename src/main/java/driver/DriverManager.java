package driver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import util.PropertiesReader;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

public class DriverManager {

    private static ThreadLocal<WebDriver> drivers = new ThreadLocal<>();

    private DriverManager() {

    }

    public static WebDriver getDriver() {
        if (Objects.isNull(drivers.get())) {
            drivers.set(createDriver());
            return drivers.get();
        } else
            return drivers.get();
    }

    private static WebDriver createDriver() {
        System.setProperty("webdriver.chrome.driver", PropertiesReader.getInstance().getChromeDriverPath());
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--incognito");
        WebDriver driver = new ChromeDriver(options);
        driver.manage().timeouts().implicitlyWait(PropertiesReader.getInstance().getImplicityWait(), TimeUnit.SECONDS);
        driver.manage().window().maximize();
        return driver;
    }

    public static void quit() {
        drivers.get().quit();
        drivers.set(null);
    }
}
