package decorator;

import org.openqa.selenium.WebElement;

public class Element {

    public WebElement getWebElement() {
        return webElement;
    }

    protected WebElement webElement;

    protected Element(WebElement webElement) {
        this.webElement = webElement;
    }

    public boolean isEnabled() {
        return webElement.isEnabled();
    }
}

