package po;

import decorator.Button;
import decorator.InputBox;
import decorator.Label;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

public class GmailHomePagePO extends BasePage {
    public GmailHomePagePO(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//div[@class='z0']/*[@role='button']")
    private Button writeAMessage;

    @FindBy(name = "to")
    private InputBox receiver;

    @FindBy(name = "subjectbox")
    private InputBox subject;

    @FindBy(xpath = "//div[@class='Ar Au']/*[@role='textbox']")
    private InputBox message;

    @FindBy(xpath = "//div[@class='dC']/*[@role='button']")
    private Button sendAMessage;

    @FindBy(id = "link_vsm")
    private Button openSentMessage;

    @FindBy(xpath = "//div[@style='display:']")
    private Label messageDetails;

    @FindBy(xpath = "//img[@alt='Закрити']")
    private Button closeMessage;

    public void clickOnWriteAMessageButton() {
        waitElementToBeClickable(writeAMessage);
        writeAMessage.click();
    }

    public void writeAMessageTo(String email) {
        receiver.sendKeys(email);
    }

    public void writeASubjectOfMessage(String subject) {
        this.subject.sendKeys(subject);
    }

    public void writeAMessage(String message) {
        this.message.sendKeys(message);
    }

    public void sendAMessage() {
        sendAMessage.click();
    }

    public void openSentMessage() {
        openSentMessage.click();
    }

    public String getMessageDetails() {
        return messageDetails.getText();
    }

    public void clickAndCloseMessage() {
        waitElementToBeClickable(closeMessage);
        new Actions(driver).clickAndHold(closeMessage.getWebElement()).release().perform();
    }
}
